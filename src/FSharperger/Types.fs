﻿namespace FSharperger
open System
open System.Collections.Concurrent
open System.Reflection
open System.Text.Json
open System.Text.Json.Serialization
open FSharp.Reflection
open TypeShape.Core

type SerializerSettings =
    {
        PropertyNameConverter : MemberInfo -> string
        CaseNameConverter: UnionCaseInfo -> string
        UnionDiscriminatorProvider: TypeInfo -> string
        EnumNameConverter: TypeInfo -> Option<FieldInfo -> string>
        UnionValueFieldName: TypeInfo -> string
    }


type Serializer<'v> = 'v -> JsonEncodedText voption -> Utf8JsonWriter -> unit     
type Deserializer<'v> = JsonElement -> 'v
    
type TypeSerializer<'t> = {
    Serializer: Serializer<'t>
    Deserializer: Deserializer<'t>
}
    
type ITypeSerializerCache =
    abstract Get<'t> : unit -> TypeSerializer<'t>
    
    
type ITypeSerialization =
    abstract CreateSerializer<'t> : SerializerSettings -> ITypeSerializerCache -> TypeSerializer<'t> option
    

    


[<AutoOpen>]    
module Utils =
    let toCamelCase str = str |> String.mapi( fun i ch -> if i = 0 then Char.ToLower(ch) else ch)
    let (|IsErasableUnion|_|) (shape:IShapeFSharpUnion) =
            if shape.UnionCases |> Array.forall (fun p -> p.Fields.Length = 0) then
                Some()
            else None
    let (|IsNoValueCase|_|) (shape:ShapeFSharpUnionCase<'t>) =
        if shape.Fields.Length = 0 then Some() else None
    let (|IsSingleValueCase|_|) (shape:ShapeFSharpUnionCase<'t>) =
        if shape.Fields.Length = 1 then Some(shape.Fields.[0]) else None
    let (|IsTupleValueCase|_|) (shape:ShapeFSharpUnionCase<'t>) =
        if shape.Fields |> Seq.forall (fun p -> p.MemberInfo.Name.StartsWith("Item")) then Some() else None
    
    


 