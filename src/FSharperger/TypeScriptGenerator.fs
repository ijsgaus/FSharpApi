module FSharperger.TypeScriptGenerator
open System
open System
open System.Linq
open System.Reflection
open System.Collections.Generic
open System.Collections.Concurrent
open System.Collections.Generic
open System.Collections.Generic
open System.Reflection
open FSharp.Reflection


[<AutoOpen>]
module Templates =
 
    let private numberTypes = [
        typeof<byte>
        typeof<sbyte>
        typeof<int16>
        typeof<uint16>
        typeof<int32>
        typeof<uint32>
        typeof<float>
        typeof<float32>
        typeof<TimeSpan>
    ]

    let private stringTypes = [
        typeof<char>
        typeof<string>
        typeof<Guid>
        typeof<byte[]>
    ] 

    let (|IsBoolean|_|) a =
        if a = typeof<bool> then Some() else None
        
    let (|IsNumber|_|) n =
        if List.contains n numberTypes then Some() else None
        
    let (|IsNumberOrString|_|) t =
        if t = typeof<int64> || t = typeof<uint64> then Some() else None

    let (|IsString|_|) t = if List.contains t stringTypes then Some() else None

    let (|IsDate|_|) t = 
        if t = typeof<DateTime> || t = typeof<DateTimeOffset> then Some() else None
        
    let (|IsTuple|_|) t =
        if t = typeof<unit> then
            Some [||]
        else
            if FSharpType.IsTuple t then
                FSharpType.GetTupleElements t |> Some
            else
                None
        
    let (|IsEnum|_|) (t :Type) =
        if t.IsEnum then Some() else None
        
    let isExactOption (t :Type) =
        t.IsConstructedGenericType && t.GetGenericTypeDefinition() = typedefof<Option<_>>
        
    let (|IsOption|_|) (t :Type) =
        if t.IsConstructedGenericType && t.GetGenericTypeDefinition() = typedefof<Option<_>> then
            Some (t.GetGenericArguments().[0])
        else
        if t.IsConstructedGenericType && t.GetGenericTypeDefinition() = typedefof<Nullable<_>> then
            Some (t.GetGenericArguments().[0])
        else
            None

    let (|IsRecord|_|) (t :Type) =
        if FSharpType.IsRecord(t) then
            Some(FSharpType.GetRecordFields(t))
        else
            None
            
    let (|IsUnion|_|) (t :Type) =
        if FSharpType.IsUnion(t) then
            Some(FSharpType.GetUnionCases(t) |> Array.map (fun p -> p, p.GetFields()))
        else
            None
            
    let (|IsErasableUnion|_|) (cases : (UnionCaseInfo * PropertyInfo[])[]) =
        if cases |> Array.forall (fun p -> (snd p).Length = 0) then
                Some()
        else None
        
    let (|IsNoValueCase|_|) (props: PropertyInfo[]) =
        if props.Length = 0 then Some() else None
    let (|IsSingleValueCase|_|) (props: PropertyInfo[]) =
        if props.Length = 1 then Some(props.[0]) else None
    let (|IsTupleValueCase|_|) (props: PropertyInfo[]) =
        if props |> Seq.forall (fun p -> p.Name.StartsWith("Item")) then Some() else None
 
type TypeDescriptor = {
    TSCode: string option
    ToPlain: string option
    FromPlain: string option
}

let noCode = { TSCode = None; ToPlain = None; FromPlain = None }
let codeOnly tsCode = { TSCode = Some tsCode; ToPlain = None; FromPlain = None }

let rec getTypeKey (typ: Type) =
    if typ.IsConstructedGenericType then
        let pos = typ.Name.IndexOf '`'
        printfn "%s" typ.Name
        let shortName = typ.Name.Substring(0, pos)
        typ.GetGenericArguments()
        |> Seq.map getTypeKey
        |> String.concat ", "
        |> sprintf "%s.%s<%s>" typ.Namespace shortName  
        
    else
        typ.FullName

let rec getTSName (settings: SerializerSettings) (typeNameConverter: TypeInfo -> string) (typ: Type) =
    match typ with
    | IsBoolean -> "boolean"
    | IsNumber -> "number"
    | IsNumberOrString -> "number | string"
    | IsString -> "string"
    | IsDate -> "Date"
    | IsEnum ->
        let typeInfo = typ.GetTypeInfo()
        match settings.EnumNameConverter typeInfo with
        | Some _ -> typeNameConverter <| typ.GetTypeInfo()
        | _ -> "number"
    | IsOption s -> getTSName settings typeNameConverter s |> sprintf "%s | null"
    | IsTuple fields ->
        fields
        |> Seq.map (fun p -> getTSName settings typeNameConverter p)
        |> String.concat ", "
        |> sprintf "[ %s ]"
    | IsRecord _ -> typeNameConverter (typ.GetTypeInfo())
    | IsUnion _ -> typeNameConverter (typ.GetTypeInfo())
    | _ -> invalidOp (sprintf "Unsupported generation type %A" typ)
    
and makeDescriptor (settings: SerializerSettings) (typeNameConverter: TypeInfo -> string) (typ: Type) =
    match typ with
    | IsBoolean -> noCode, []
    | IsNumber -> noCode, []
    | IsNumberOrString -> noCode, []
    | IsString -> noCode, []
    | IsDate -> 
        {
          TSCode = None
          FromPlain = Some "(val:any, decoder: Decoder) => (new Date(val))"
          ToPlain = Some "(val:Date, decoder: Decoder) => (val.toISOString())" 
        }, []
    | IsEnum ->
        let typeInfo = typ.GetTypeInfo()
        let fields = typeInfo.GetFields(BindingFlags.Static ||| BindingFlags.Public)
        let typeName = typeNameConverter typeInfo
        match settings.EnumNameConverter typeInfo with
        | Some cvt ->
            let names = fields |> Array.map cvt |> Seq.map (sprintf "\"%s\"")
            let code = names |> String.concat " | " |> sprintf "export type %s = %s;" typeName 
            codeOnly code, []
        | None ->
            let consts =
                fields
                |> Seq.map (fun p -> p.Name, Convert.ToInt64(p.GetValue null))
                |> Seq.map (fun (n, v) -> sprintf "    %s: %d," n v)
                |> String.concat Environment.NewLine
            let decl = sprintf "export const %s = {\n%s\n};" typeName consts
            codeOnly decl, []
    | IsOption s ->
        let innerKey = getTypeKey s
        {
            TSCode = None
            FromPlain = sprintf "(val, decoder) => (val === null? null: decoder('%s', val))" innerKey |> Some
            ToPlain = sprintf "(val, decoder) => (val === null? null: decoder('%s', val))" innerKey |> Some
        }, [s]
    | IsTuple fields ->
        let keys = fields |> Seq.map getTypeKey |> Seq.map (sprintf "'%s'") |> String.concat ", "
        {
            TSCode = None
            FromPlain = sprintf "decodeTuple([%s])" keys |> Some
            ToPlain = sprintf "decodeTuple([%s])" keys |> Some
        }, fields |> List.ofArray
    | IsRecord fields ->
        let normalized =
            fields
                |> Seq.map (fun p ->
                                {|
                                   PropertyName = settings.PropertyNameConverter (p :> MemberInfo)
                                   TypeKey  = getTypeKey p.PropertyType
                                   IsOptional = if isExactOption p.PropertyType then "?" else ""
                                   TypeName = getTSName settings typeNameConverter p.PropertyType
                                |})
                |> List.ofSeq                
                
                
        let decl =
            normalized
            |> Seq.map (
                           fun p ->
                                   sprintf "    %s%s : %s" p.PropertyName p.IsOptional p.TypeName
                       )
            |> String.concat ";\n"
            |> sprintf "export interface %s {\n%s;\n};" (getTSName settings typeNameConverter typ)
        let fieldMap =
            normalized
            |> Seq.map (fun p -> sprintf "%s:'%s'" p.PropertyName p.TypeKey)
            |> String.concat ", "
            |> sprintf "{ %s }"
        {
            TSCode = decl |> Some
            FromPlain = sprintf "decodeMap(%s)" fieldMap |> Some
            ToPlain = sprintf "decodeMap(%s)" fieldMap |> Some
        }, (fields |> List.ofArray |> List.map (fun p -> p.PropertyType))
    | IsUnion cases ->
        match cases with
        | IsErasableUnion ->
             
            { TSCode =  }
    | _ -> invalidOp (sprintf "Unsupported generation type %A" typ)
    
let generateTypes (startWith: Type list) mkDesc =
    let rec genTypes (toGen : HashSet<Type>) (map: Dictionary<Type, TypeDescriptor>) =
        if toGen.All(fun p -> map.ContainsKey(p)) then
            map
        else
            for typ in toGen |> Seq.filter (fun p -> map.ContainsKey(p) |> not) |> List.ofSeq do
                let desc, add = mkDesc typ
                add |> List.iter (fun p -> toGen.Add(p) |> ignore)
                map.[typ] <- desc
            genTypes toGen map
    genTypes (HashSet(startWith)) (Dictionary())
    


let generate settings typeNameConverter types =
    let desc = generateTypes types (makeDescriptor settings typeNameConverter)
    let decodeTable =
        desc
        |> Seq.map (fun p -> getTypeKey p.Key, p.Value)
        |> Seq.map (fun p ->
                        sprintf "    '%s' : { toPlain: %s, fromPlain: %s }"
                            (fst p)
                            ((snd p).ToPlain |> Option.defaultValue "id")
                            (((snd p).FromPlain |> Option.defaultValue "id")))
        |> String.concat ",\n "
        |> sprintf "const decoderMap: DecoderMap = {\n%s\n};\n"
    let types =
        desc
        |> Seq.choose (fun p -> p.Value.TSCode)
        |> String.concat "\n\n"
    decodeTable + types
    
     