namespace FSharpApi.TypeScript
open System
open FSharpApi
open System.Collections.Generic

type TSTypeName =
    {
        Namespace: string
        Name: string
    }
    member __.FullName =
        match __.Namespace with
        | "" -> __.Name
        | _ -> sprintf "%s.%s" __.Namespace __.Name

 

type GenerationSettings = {
    Serializer: SerializerSettings
    TypeNameConverter: Type -> TSTypeName
}

type TSTypeInfo = {
    Name: TSTypeName
    NeedDecode: bool
}

type TypeNameProvider = Type -> TSTypeInfo

type TypeGenerator = {
    Collect: Type -> Option<Type list>
    ProvideName: GenerationSettings -> TypeNameProvider -> Type -> TSTypeInfo
    Generate: Type -> GenerationSettings -> TypeNameProvider -> CodeWriter -> CodeWriter 
}

module TypeGenerator =
    module List =
        let rec tryChoose f list =
            match list with
            | [] -> None
            | h::t ->
                match f h with
                | Some v -> Some(v, h)
                | None -> tryChoose f t
    
    let generateTypes settings providers cw types =
        let types = HashSet(types |> Seq.ofList)
        let typeProviderMap = Dictionary()
        while types.Count > 0 do
            let typ = types |> Seq.head
            let add, prov =
                providers
                |> List.tryChoose (fun p -> p.Collect(typ))
                |> Option.defaultWith (fun () -> invalidOp (sprintf "Cannot generate type %s" typ.FullName))
            
            add |> List.iter (fun p -> types.Add(p) |> ignore)
            types.Remove(typ) |> ignore
            typeProviderMap.Add(typ, prov)
        let rec tnp typ =
            typeProviderMap.[typ].ProvideName settings tnp typ
        tnp,
        typeProviderMap
        |> Seq.fold (fun cw p -> p.Value.Generate p.Key settings tnp cw) cw
        
                
            

