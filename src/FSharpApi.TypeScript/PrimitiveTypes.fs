module FSharpApi.TypeScript.PrimitiveTypes
open System.Xml

let numbers = {
    Collect =
        fun typ ->
            match typ with
            | IsNumber -> Some []
            | _ -> None
    ProvideName =
        fun _ _ typ -> { Name = { Namespace = ""; Name = "number" }; NeedDecode = false }
    Generate =
        fun _ _ _ cw -> cw
}

let bool = {
    Collect =
        fun typ ->
            match typ with
            | IsBoolean -> Some []
            | _ -> None
    ProvideName =
        fun _ _ typ -> { Name = { Namespace = ""; Name = "boolean" }; NeedDecode = false }
    Generate =
        fun _ _ _ cw -> cw
}

let longs = {
    Collect =
        fun typ ->
            match typ with
            | IsNumberOrString -> Some []
            | _ -> None
    ProvideName =
        fun _ _ typ -> { Name = { Namespace = ""; Name = "number | string" }; NeedDecode = false }
    Generate =
        fun _ _ _ cw -> cw
}

let strings = {
    Collect =
        fun typ ->
            match typ with
            | IsString -> Some []
            | _ -> None
    ProvideName =
        fun _ _ typ -> { Name = { Namespace = ""; Name = "string" }; NeedDecode = false }
    Generate =
        fun _ _ _ cw -> cw
}

let dates = {
    Collect =
        fun typ ->
            match typ with
            | IsDate -> Some []
            | _ -> None
    ProvideName =
        fun _ _ typ -> { Name = { Namespace = ""; Name = "Date" }; NeedDecode = false }
    Generate =
        fun _ _ _ cw ->
            cw
            |> CodeWriter.addDecoder "Date" ["$dateDecoder"]
            |> CodeWriter.addEncoder "Date" ["$dateEncoder"]
}