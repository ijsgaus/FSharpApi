namespace FSharpApi.TypeScript
open System.IO

module CodeWriter =
    type NameSpace = private {
        NameSpaces: Map<string, NameSpace>
        Lines: string list
    }
    
    type CodeWriterSettings = {
        EncodeConst: string
        DecodeConst: string
        Tab: string
    }    
    
    type CodeWriter = private {
        Settings: CodeWriterSettings
        Root: NameSpace
        DecodeTable: Map<string, string list>
        EncodeTable: Map<string, string list>
    }
        
    let create settings = {
            Settings = settings 
            Root = { NameSpaces = Map.empty; Lines = [] }
            DecodeTable = Map.empty
            EncodeTable = Map.empty
    }
    
    let addLines (ns: string) lines cw =
        let rec add lst ns =
            match lst with
            | [] -> { ns with Lines = ns.Lines @ lines }
            | h::t ->
                let next = ns.NameSpaces |> Map.tryFind h |> Option.defaultValue { NameSpaces = Map.empty; Lines = [] } 
                { ns with NameSpaces = ns.NameSpaces |> Map.add h (add t next) }                
        let nsList = ns.Split('.') |> Seq.map (fun s -> s.Trim()) |> Seq.filter ((=) "") |> Seq.toList
        { cw with Root = add nsList cw.Root }
        
        
    let addEncoder name text cw =
        { cw with EncodeTable = cw.EncodeTable |> Map.add name text }
        
    let getTab cw = cw.Settings.Tab
        
    let addDecoder name text cw =
        { cw with DecodeTable = cw.DecodeTable |> Map.add name text }
        
    let tabify (tab: string) list =
        list |> List.map (fun p -> tab + p)
        
    let write cw (writer: TextWriter) =
        let settings = cw.Settings
        let rec writeNS name ns (outer: string)  =
            let inner =
                if name <> "" then
                    writer.Write(outer)
                    writer.WriteLine(sprintf "export namespace %s {" name)
                    outer + settings.Tab
                else outer
            ns.NameSpaces |> Map.iter (fun nm ns -> writeNS nm ns inner)
            ns.Lines |> tabify inner |> List.iter (fun str -> writer.WriteLine(str))
            if name <> "" then
                writer.Write(outer)
                writer.WriteLine("}")
        let writeTable name table =
            let writeEntry nm code =
                let rec writeLine lst =
                    match lst with
                    | [] -> ()
                    | [l] -> writer.WriteLine(sprintf "%s%s%s," settings.Tab settings.Tab l)
                    | h::t ->
                        writer.WriteLine(sprintf "%s%s%s" settings.Tab settings.Tab h)
                        writeLine t
                match code with
                | [] -> ()
                | [s] ->
                    writer.WriteLine(sprintf "%s'%s': %s," settings.Tab nm s)
                | _ ->
                    writer.WriteLine(sprintf "%s'%s':" settings.Tab nm)
                    writeLine code
            writer.WriteLine(sprintf "export const %s = {" name)
            table |> Map.iter writeEntry
            writer.WriteLine("}")        
        writeNS "" cw.Root "" 
        writer.WriteLine()
        writeTable settings.EncodeConst cw.EncodeTable
        writer.WriteLine()
        writeTable settings.DecodeConst cw.DecodeTable

type CodeWriter = CodeWriter.CodeWriter        
        
     
    
     
        
