module FSharpApi.TypeScript.Maps

let generator = {
    Collect =
        fun typ ->
            match typ with
            | IsMapLike (StringMap t) ->
                Some [ t ]
            | IsMapLike (CustomMap (tk, tv)) ->
                Some [ tk; tv ]
            | _ -> None
    ProvideName =
        fun settings np typ ->
            let mapLike =
                (|IsMapLike|_|) typ
                |> Option.defaultWith (fun () -> invalidOp "Cannot be here")
            match mapLike with
            | StringMap t ->
                let t = np t;
                { Name = { Namespace = ""; Name = sprintf "{ [key: string]: %s }" t.Name.FullName }; NeedDecode = t.NeedDecode }
            | CustomMap (tk, tv) ->
                let tk = np tk
                let tv = np tv
                { Name = { Namespace = ""; Name = sprintf "Map<%s, %s>" tk.Name.FullName tv.Name.FullName }; NeedDecode = true }
    Generate =
        fun typ settings np cw ->
            let mapLike =
                (|IsMapLike|_|) typ
                |> Option.defaultWith (fun () -> invalidOp "Cannot be here")
            let myName = np typ
            match mapLike with
            | StringMap t ->
                let t = np t;
                if t.NeedDecode then
                    let code = sprintf "$stringMapCoder(%s)" t.Name.FullName 
                    cw
                    |> CodeWriter.addEncoder myName.Name.FullName [code]
                    |> CodeWriter.addDecoder myName.Name.FullName [code]
                else cw
            | CustomMap (tk, tv) ->
                let makeParam ti =
                    if ti.NeedDecode then sprintf "'%s'" ti.Name.FullName else "null"
                let kn = np tk |> makeParam
                let vn = np tv |> makeParam
                let encode = sprintf "$es6MapEncoder(%s, %s)" kn vn
                let decode = sprintf "$es6MapDecoder(%s, %s)" kn vn
                cw
                |> CodeWriter.addEncoder myName.Name.FullName [encode]
                |> CodeWriter.addDecoder myName.Name.FullName [decode]
}