type $Coder = (value: any, map: $CoderMap) => any;
type $CoderMap = { [kind: string] : $Coder };

const $dateDecoder : $Coder = 
    (val: any, map: $CoderMap) => (new Date(val));
const $dateEncoder : $Coder =
    (val: any, map: $CoderMap) => (val.toISOString());

const $optionCoder = (key: string) : $Coder => (value: any, map: $CoderMap) => {
    if(value) {
        return map[key](value, map);
    }
    else
        return null;
};

const $es6MapDecoder = (keyName: string | null, valueName: string | null) : $Coder =>
    (input: any, map: $CoderMap) => {
        if(keyName || valueName)
        {
            const mapI = new Map();
            for(let [key, value] of input) {
                if(keyName)
                    key = map[keyName](key, map);
                if(valueName)
                    value = map[valueName](value, map)
                mapI.set(key, value);
            }
            return <any> mapI;
        }
        else
            return new Map(input);
    };

const $es6MapEncoder = (keyName: string | null, valueName: string | null) : $Coder =>
    (input: any, map: $CoderMap) => {
        const mapI = <Map<any, any>> input;
        if (keyName || valueName ) {
            const res = [];
            for(let [key, value] of mapI.entries()) {
                if(keyName)
                    key = map[keyName](key, map);
                if(valueName)
                    value = map[valueName](value, map)
                res.push([key, value]);
            }
            return res;
        }
        else
            return [...mapI.entries() ];
    };

const $es6SetDecoder = (valueName: string | null) : $Coder =>
    (input: any, map: $CoderMap) => {
        if(valueName)
            return new Set(input.map((v : any) => (map[valueName](v, map))));
        else
            return new Set(input);
    };

const $es6SetEncoder = (valueName: string | null) : $Coder =>
    (input: any, map: $CoderMap) => {
        const set = <Set<any>> input;
        if (valueName ) {
            return [...set.entries()].map(([_, v]) => (map[valueName](v, map)));
        }
        else
            return [...set.entries() ].map(([_, v]) => v);
    };

const $stringMapCoder = (valueName: string) : $Coder =>
    (input: any, map: $CoderMap) => {
        for(let key of Object.getOwnPropertyNames(input)) {
            input[key] = map[valueName](input[key], map);
        }
        return input;
    };

export const A   = 5;

/*
const decodeTuple = (keys: string[]) => (input: any, decoder: Decoder) => {
    for(let i = 0; i < input.length; i++)
        input[i] = decoder(keys[i], input[i]);
    return input;
};

const decodeArray = (key: string) => (input: any, decoder: Decoder) => {
    for(let i = 0; i < input.length; i++)
        input[i] = decoder(key, input[i]);
    return input;
};

const decodeMap = (keyMap: { [prop: string] : string }) => (input: any, decoder: Decoder) => {
    for(let key in keyMap) {
        if(input[key] !== undefined) {
            input[key] = decoder(keyMap[key], input[key])
        }
    }
    return input;
};

const decodeSingleUnion = (vName: string, vType: string ) => (input: any, decoder: Decoder) => {
  input[vName] = decoder(vType, input[vName])
  return input;
};

const decodeTupleUnion = (vName: string, vTypes: string[] ) => (input: any, decoder: Decoder) => {
    for(let i = 0; i < input[vName].length; i++)
        input[vName][i] = decoder(vTypes[i], input[vName][i]);
    return input;
}; 

const decodeUnion = (dName: string, caseMap: { [prop: string] : (input: any, decoder: Decoder) => any }) =>
    (input: any, decoder: Decoder) => (caseMap[input[dName]](input, decoder));

const decodeES6Map = (keyName: string, valueName: string) =>
    (input: any, decoder: Decoder) => {
        const map = new Map();
        for(let [key, value] of input) {
            const keyV = decoder(keyName, key);
            const valV = decoder(valueName, value);
            map.set(keyV, valV);
        }
        return <any> map;
    };

const encodeES6Map = (keyName: string, valueName: string) =>
    (input: any, decoder: Decoder) => {
        const map = <Map<any, any>> input;
        const res = []; 
        for(let [key, value] of map.entries()) {
            const keyV = decoder(keyName, key);
            const valV = decoder(valueName, value);
            res.push([keyV, valV]);
        }
        return <any> res;
    };

const decodeStringMap = (valueName: string) =>
    (input: any, decoder: Decoder) => {
        for(let key of Object.getOwnPropertyNames(input)) {
            input[key] = decoder(valueName, input[key]);
        }
        return input;
    };


const decodeES6Set = (valueName: string) =>
    (input: any, decoder: Decoder) => {
        const set = new Set();
        for(let value of input) {
            const valV = decoder(valueName, value);
            set.add(valV);
        }
        return <any> set;
    };

const encodeES6Set = (valueName: string) =>
    (input: any, decoder: Decoder) => {
        const set = <Set<any>> input;
        const res = [];
        for(let [_, value] of set.entries()) {
            const valV = decoder(valueName, value);
            res.push(valV);
        }
        return <any> res;
    };


export const toPlain = (code: string, input: any) : any => (decoderMap[code].toPlain(input, toPlain));

export const fromPlain = (code: string, input: any) : any => (decoderMap[code].fromPlain(input, fromPlain));

export interface Record {
    field1 : number;
    field2 : [ string, Enum1 ];
    field3? : number;
    field4 : [ number, Record | null ];
};
export type Enum1 = "one" | "two";
export const Enum2 = {
    One: 1,
    Two: 2,
};
export type U1 = 'a' | 'b' | 'c' | 'decimal';
interface UnionCase0 {
    $type: 'case0';
}

interface UnionCase1 {
    $type: 'case1';
    payload: [number, boolean];
}

interface UnionCase2 {
    $type: 'case2';
    me : number;
    other : string;
};

interface UnionCase3 {
    $type: 'case3';
    payload: Record;
}

interface UnionCase4 {
    $type: 'case4';
    payload: Array<number>;
}


export type Union = UnionCase0 | UnionCase1 | UnionCase2 | UnionCase3 | UnionCase4;
const decoderMap: DecoderMap = {
    'Record': {
        toPlain:
            decodeMap(
                {
                    field1:'number',
                    field2:'[ string, Enum1 ]',
                    field3:'number | null',
                    field4:'[ number, Record | null ]',
                })
        ,
        fromPlain:
            decodeMap(
                {
                    field1:'number',
                    field2:'[ string, Enum1 ]',
                    field3:'number | null',
                    field4:'[ number, Record | null ]',
                })
    },
    'number': {
        toPlain: id,
        fromPlain: id
    },
    '[ string, Enum1 ]': {
        toPlain:
            decodeTuple(['string', 'Enum1'])
        ,
        fromPlain:
            decodeTuple(['string', 'Enum1'])
    },
    'string': {
        toPlain:
            (val:any, decoder: Decoder) => (val.toISOString())
        ,
        fromPlain:
            (val:any, decoder: Decoder) => (new Date(val))
    },
    'Enum1': {
        toPlain: id,
        fromPlain: id
    },
    'number | null': {
        toPlain:
            (val, decoder) => (val === null? null: decoder('number', val))
        ,
        fromPlain:
            (val, decoder) => (val === null? null: decoder('number', val))
    },
    '[ number, Record | null ]': {
        toPlain:
            decodeTuple(['number', 'Record | null'])
        ,
        fromPlain:
            decodeTuple(['number', 'Record | null'])
    },
    'Record | null': {
        toPlain:
            (val, decoder) => (val === null? null: decoder('Record', val))
        ,
        fromPlain:
            (val, decoder) => (val === null? null: decoder('Record', val))
    },
    'U1': {
        toPlain: id,
        fromPlain: id
    },
    'Union': {
        toPlain:
            decodeUnion('$type',
                {
                    'case0':
                    id,
                    'case1':
                        decodeTupleUnion('payload', ['number', 'boolean']),
                    'case2':
                        decodeMap(
                            {
                                me:'number',
                                other:'string',
                            }),
                    'case3':
                        decodeSingleUnion('payload', 'Record'),
                    'case4':
                        decodeSingleUnion('payload', 'Array<number>'),
                }
            )
        ,
        fromPlain:
            decodeUnion('$type',
                {
                    'case0':
                    id,
                    'case1':
                        decodeTupleUnion('payload', ['number', 'boolean']),
                    'case2':
                        decodeMap(
                            {
                                me:'number',
                                other:'string',
                            }),
                    'case3':
                        decodeSingleUnion('payload', 'Record'),
                    'case4':
                        decodeSingleUnion('payload', 'Array<number>'),
                }
            )
    },
    'boolean': {
        toPlain: id,
        fromPlain: id
    },
    'Array<number>': {
        toPlain:
            decodeArray('number')
        ,
        fromPlain:
            decodeArray('number')
    },
};*/