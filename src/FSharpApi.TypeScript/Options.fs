module FSharpApi.TypeScript.Options

let generator = {
    Collect =
        fun typ ->
            match typ with
            | IsOption s -> Some [ s.Inner ]
            | _ -> None
    ProvideName =
        fun settings np typ ->
            let inner =
                (|IsOption|_|) typ
                |> Option.defaultWith (fun () -> invalidOp "Cannot be here")
            let inner = np inner.Inner
            { Name = { Namespace = ""; Name = sprintf "%s | null" inner.Name.FullName }; NeedDecode = inner.NeedDecode }
     
    Generate =
        fun typ settings np cw ->
            let inner =
                (|IsOption|_|) typ
                |> Option.defaultWith (fun () -> invalidOp "Cannot be here")
            let inner = np inner.Inner
            if inner.NeedDecode then
                let code = [sprintf "$optionCoder(%s)" inner.Name.FullName]
                let myName = (np typ).Name.FullName
                cw
                |> CodeWriter.addDecoder myName code
                |> CodeWriter.addEncoder myName code
            else cw
}