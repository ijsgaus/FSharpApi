[<AutoOpen>]
module FSharpApi.TypeScript.Generator
open System
open System
open System.Collections.Generic
open System.Collections.Generic
open FSharpApi

let rec getTypeKey (typ: Type) =
    if typ.IsConstructedGenericType then
        let pos = typ.Name.IndexOf '`'
        printfn "%s" typ.Name
        let shortName = typ.Name.Substring(0, pos)
        typ.GetGenericArguments()
        |> Seq.map getTypeKey
        |> String.concat ", "
        |> sprintf "%s.%s<%s>" typ.Namespace shortName  
        
    else
        typ.FullName



let generateTSTypes settings (nameGen: TypeNameGenerator) (codeGen: TypeCodeGenerator<string>) types =
    let types = types |> List.distinct
    let rec np typ =
        match nameGen settings np typ with
        | Some name -> name
        | None -> invalidArg "typ" (sprintf "Unknown type %O" typ)
    let genOne typ =
        match codeGen settings np typ with
        | Some v -> v
        | None -> invalidArg "typ" (sprintf "Unknown type %O" typ)
    let rec genCodes (complete: Dictionary<_, _>) toGen =
        let toGen =
            toGen
            |> List.distinct
            |> List.filter (fun p -> complete.ContainsKey p |> not)
            
        match toGen with
        | [] -> complete
        | [h] ->
            let code, types = genOne h
            complete.Add(h, code)
            match types with
            | [] -> complete
            | _ -> genCodes complete types
        | h::t ->
            let code, types = genOne h
            complete.Add(h, code)
            let t = List.append types t
            genCodes complete t
    let generated = genCodes (Dictionary()) types
    let declarations =
        generated.Values
        |> Seq.collect (fun p -> p.Code)
        |> String.concat Environment.NewLine
    let tab = sprintf "    %s"
    let makeDecoderEntry t =
        let v = generated.[t]
        [
          yield sprintf "'%s': {" (np t)
          match v.ToPlain with
          | [] -> yield tab "toPlain: id,"
          | _ ->
              yield tab "toPlain:"
              yield! v.ToPlain |> List.map (tab >> tab)
              yield tab ","
          match v.FromPlain with
          | [] -> yield tab "fromPlain: id"
          | _ ->
              yield tab "fromPlain:"
              yield! v.FromPlain |> List.map (tab >> tab)
          yield "},"      
        ]
    let table =
        [
            yield "const decoderMap: DecoderMap = {"
            yield!
                generated.Keys
                |> Seq.map (fun p -> np p, p)
                |> Seq.distinctBy fst
                |> Seq.map snd
                |> Seq.collect makeDecoderEntry
                |> Seq.map tab
            yield "};"
        ] |> String.concat Environment.NewLine
    declarations + Environment.NewLine + table
            
            