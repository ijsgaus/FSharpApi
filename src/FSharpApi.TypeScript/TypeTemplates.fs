[<AutoOpen>]
module FSharpApi.TypeScript.TypeTemplates
open System
open System.Reflection
open System.Collections.Generic


open FSharp.Reflection
let private numberTypes = [
        typeof<byte>
        typeof<sbyte>
        typeof<int16>
        typeof<uint16>
        typeof<int32>
        typeof<uint32>
        typeof<float>
        typeof<float32>
        typeof<TimeSpan>
    ]

let private stringTypes = [
    typeof<char>
    typeof<string>
    typeof<Guid>
    typeof<byte[]>
]

let (|IsBoolean|_|) a =
        if a = typeof<bool> then Some() else None
        

    
        
let (|IsNumber|_|) n =
    if List.contains n numberTypes then Some() else None
    
let (|IsNumberOrString|_|) t =
    if t = typeof<int64> || t = typeof<uint64> then Some() else None

let (|IsString|_|) t = if List.contains t stringTypes then Some() else None

let (|IsDate|_|) t = 
    if t = typeof<DateTime> || t = typeof<DateTimeOffset> then Some() else None
    
let (|IsTuple|_|) t =
    if t = typeof<unit> then
        Some [||]
    else
        if FSharpType.IsTuple t then
            FSharpType.GetTupleElements t |> Some
        else
            None
    
let (|IsEnum|_|) (t :Type) =
    if t.IsEnum then Some() else None
    
    
type OptionLike =
    | SkipInMaps of Type
    | NullInMaps of Type
    member __.Inner =
        match __ with | SkipInMaps s -> s | NullInMaps s -> s
        
let (|IsOption|_|) (t :Type) =
    if t.IsConstructedGenericType && t.GetGenericTypeDefinition() = typedefof<Option<_>> then
        t.GetGenericArguments().[0] |> SkipInMaps |> Some
    else
    if t.IsConstructedGenericType && t.GetGenericTypeDefinition() = typedefof<Nullable<_>> then
        t.GetGenericArguments().[0] |> NullInMaps |> Some
    else
        None
        
let (|IsArrayLike|_|) (t :Type) =
    if t.IsConstructedGenericType && t.GetGenericTypeDefinition() = typedefof<Microsoft.FSharp.Collections.List<_>> then
        t.GetGenericArguments().[0] |> Some
    elif t.IsArray  then
        t.GetElementType() |> Some
    else None
    
type MapLike =
    | StringMap of Type
    | CustomMap of Type * Type
    
let (|IsMapLike|_|) (t: Type) =
    if t.IsConstructedGenericType then
       let typeDef = t.GetGenericTypeDefinition()
       if typeDef = typedefof<Map<_, _>> || typeDef = typedefof<Dictionary<_, _>> then
           let args = t.GetGenericArguments()
           if args.[0] = typeof<string> then
               Some(StringMap args.[1])
           else
               Some <| CustomMap (args.[0], args.[1])
       else
           None
    else
        None
        
let (|IsSetLike|_|) (t: Type) =
    if t.IsConstructedGenericType then
       let typeDef = t.GetGenericTypeDefinition()
       if typeDef = typedefof<Set<_>> || typeDef = typedefof<HashSet<_>> then
           let args = t.GetGenericArguments()
           Some(args.[0])
       else
           None
    else
        None        
           
        
let (|IsRecord|_|) (t :Type) =
    if FSharpType.IsRecord(t) then
        Some(FSharpType.GetRecordFields(t))
    else
        None


        

type CaseKind =
    | NoValue of UnionCaseInfo
    | SingleValue of UnionCaseInfo * PropertyInfo
    | TupleValue of UnionCaseInfo * PropertyInfo[]
    | NamedValues of UnionCaseInfo * PropertyInfo[]

type UnionKind =
    | Erased of UnionCaseInfo[]
    | NotErased of CaseKind[]
        
let (|IsUnion|_|) t =
    if FSharpType.IsUnion(t) then
       let cases = FSharpType.GetUnionCases(t) |> Array.map (fun p -> p, p.GetFields())
       if cases |> Array.forall (fun p -> (snd p).Length = 0) then
           cases |> Array.map (fun p -> fst p) |> Erased |> Some
       else
           let mapCase (case: UnionCaseInfo, fields: PropertyInfo[]) =
              match fields with
              | [||]  -> NoValue case
              | [| v |] -> SingleValue(case, v)
              | a when a |> Array.forall (fun p -> p.Name.StartsWith("Item")) ->
                  TupleValue(case, a)
              | _ ->
                  NamedValues(case, fields)
           cases |> Array.map mapCase |> NotErased |> Some 
    else
        None
            
    
        
    