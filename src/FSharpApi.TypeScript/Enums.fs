module FSharpApi.TypeScript.Enums
open System.Reflection
open System

let generator = {
    Collect =
        fun typ ->
            match typ with
            | IsEnum -> Some []
            | _ -> None
            
    ProvideName =
        fun settings _ typ ->
            match settings.Serializer.EnumNameConverter typ with
            | Some _ -> { Name = settings.TypeNameConverter <| typ; NeedDecode = false }
            | _ -> { Name = { Namespace = ""; Name = "number" }; NeedDecode = false }
    Generate =
        fun typ settings np cw ->
            let fields = typ.GetFields(BindingFlags.Static ||| BindingFlags.Public)
            let typeName = np typ
            match settings.Serializer.EnumNameConverter typ with
            | Some cvt ->
                let names = fields |> Array.map cvt |> Seq.map (sprintf "\"%s\"")
                
                let code = names |> String.concat " | " |> sprintf "export type %s = %s;" typeName.Name.Name
                cw |> CodeWriter.addLines typeName.Name.Namespace [code]
                
            | None ->
                let shift = CodeWriter.getTab cw
                let code = [
                    yield sprintf "export const %s = {" typeName.Name.Name
                    yield!
                        fields
                        |> Seq.map (fun p -> p.Name, Convert.ToInt64(p.GetValue null))
                        |> Seq.map (fun (n, v) -> sprintf "%s%s: %d," shift n v)
                    yield "}"    
                ]
                cw |> CodeWriter.addLines typeName.Name.Namespace code
}