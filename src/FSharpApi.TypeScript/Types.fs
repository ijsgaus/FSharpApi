namespace FSharpApi.TypeScript

open FSharpApi
open System
open System.Reflection

type GenerationSettings = {
    Serializer: SerializerSettings
    TypeNameConverter: Type -> string
}

type TypeDescriptor<'t> = {
    Code: 't list
    ToPlain: 't list
    FromPlain: 't list
}

module TypeDescriptor =
    let noCode = { Code = []; ToPlain = []; FromPlain = [] }
    let codeOnly code = { Code = code; ToPlain = []; FromPlain = [] }
    

type TypeNameProvider = TypeNameProvider of (Type -> string) 

type TypeGenerator = {
    Collect: Type -> Option<Type list>
    ProvideName: GenerationSettings -> TypeNameProvider -> Type -> string
    Generate: Type -> TypeNameProvider -> CodeWriter -> unit 
}


type NameProvider = Type -> string
type TypeNameGenerator = GenerationSettings -> NameProvider -> Type -> string option

type CodeGeneratorResult<'t> = TypeDescriptor<'t> * Type list

type TypeCodeGenerator<'t> = GenerationSettings -> NameProvider -> Type -> CodeGeneratorResult<'t> option 



    