module FSharpApi.TypeScript.Primitives
open System
open System
open System
open System
open System
open System.ComponentModel
open System.Reflection
open FSharp.Reflection


let nameBool (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsBoolean -> Some ("boolean") 
    | _ -> None
    
let genBool (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsBoolean -> Some(TypeDescriptor.noCode, []) 
    | _ -> None
    
let nameNumber (settings:GenerationSettings) (np: NameProvider) typ  : string option =
    match typ with
    | IsNumber -> Some ("number")
    | _ -> None
    
let genNumber (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsNumber ->  Some(TypeDescriptor.noCode, []) 
    | _ -> None
    
let nameNumberOrString (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsNumberOrString -> Some ("number | string")
    | _ -> None
    
let genNumberOrString (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsNumberOrString ->  Some(TypeDescriptor.noCode, []) 
    | _ -> None
    
let nameString (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsString -> Some ("string")
    | _ -> None
    
let genString (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsString ->  Some(TypeDescriptor.noCode, []) 
    | _ -> None
    
let nameDate (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsDate -> Some ("Date")
    | _ -> None
    
let genDate (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsString ->
        let descriptor =
            {
                Code = []
                FromPlain = ["(val:any, decoder: Decoder) => (new Date(val))"]
                ToPlain = ["(val:any, decoder: Decoder) => (val.toISOString())"] 
            }
        Some(descriptor, []) 
    | _ -> None
    
let nameEnum (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsEnum ->
        match settings.Serializer.EnumNameConverter typ with
        | Some _ -> Some (settings.TypeNameConverter <| typ)
        | _ -> Some("number")
    | _ -> None
        
let genEnum (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsEnum ->
        let typeInfo = typ.GetTypeInfo()
        let fields = typeInfo.GetFields(BindingFlags.Static ||| BindingFlags.Public)
        let typeName = settings.TypeNameConverter typ
        match settings.Serializer.EnumNameConverter typ with
        | Some cvt ->
            let names = fields |> Array.map cvt |> Seq.map (sprintf "\"%s\"")
            let code = names |> String.concat " | " |> sprintf "export type %s = %s;" typeName 
            TypeDescriptor.codeOnly [code], []
        | None ->
            let constants =
                fields
                |> Seq.map (fun p -> p.Name, Convert.ToInt64(p.GetValue null))
                |> Seq.map (fun (n, v) -> sprintf "    %s: %d," n v)
                |> String.concat Environment.NewLine
            let decl = sprintf "export const %s = {\n%s\n};" typeName constants
            TypeDescriptor.codeOnly [decl], []
        |> Some
    | _ -> None
    
let nameOption (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsOption s ->
        Some(np s.Inner + " | null")
    | _ -> None
    
let genOption (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsOption s ->
        let innerKey = np s.Inner
        Some({
            Code = []
            FromPlain = [sprintf "(val, decoder) => (val === null? null: decoder('%s', val))" innerKey]
            ToPlain = [sprintf "(val, decoder) => (val === null? null: decoder('%s', val))" innerKey]
        }, [s.Inner])
    | _ -> None
    
let nameMap (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsMapLike (StringMap t) ->
        Some(sprintf "{ [key: string]: %s }" (np t))
    | IsMapLike (CustomMap (tk, tv)) ->
        Some(sprintf "Map<%s, %s>" (np tk) (np tv))
    | _ -> None
    
let genMap (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsMapLike (StringMap t) ->
        Some({
            Code = []
            FromPlain = [
                sprintf "decodeStringMap('%s')" (np t)
            ]
            ToPlain = [
                sprintf "decodeStringMap('%s')" (np t)
            ]
        }, [t])
    | IsMapLike (CustomMap (tk, tv)) ->
        Some({
            Code = []
            FromPlain = [
                sprintf "decodeES6Map('%s', '%s')" (np tk) (np tv)
            ]
            ToPlain = [
                sprintf "encodeES6Map('%s', '%s')" (np tk) (np tv)
            ]
        }, [tk; tv])
    | _ -> None
    
let nameSet (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsSetLike t ->
        Some(sprintf "Set<%s>" (np t))
    | _ -> None
    
let genSet (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsSetLike t ->
        Some({
            Code = []
            FromPlain = [
                sprintf "decodeES6Set('%s')" (np t)
            ]
            ToPlain = [
                sprintf "encodeES6Set('%s')" (np t)
            ]
        }, [t])
    | _ -> None    
    
let nameTuple (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsTuple fields ->
        fields
        |> Seq.map (fun p -> np p)
        |> String.concat ", "
        |> sprintf "[ %s ]"
        |> Some
    | _ -> None
    
let genTuple (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsTuple fields ->
        let keys = fields |> Seq.map np |> Seq.map (sprintf "'%s'") |> String.concat ", "
        Some({
            Code = []
            FromPlain = [sprintf "decodeTuple([%s])" keys]
            ToPlain = [sprintf "decodeTuple([%s])" keys]
        }, fields |> List.ofArray)
    | _ -> None
    
    
let nameRecord (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsRecord _ -> settings.TypeNameConverter typ |> Some
    | _ -> None
    
let genRecord (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsRecord fields ->
        let normalized =
            fields
                |> Seq.map (fun p ->
                                let opt, pt =
                                    match p.PropertyType with
                                       | IsOption o ->
                                           match o with
                                           | SkipInMaps s -> "?", np s 
                                           | _ -> "", np p.PropertyType
                                       | _ -> "", np p.PropertyType
                                {|
                                   PropertyName = settings.Serializer.MemberNameConverter (p :> MemberInfo)
                                   TypeKey  = np p.PropertyType
                                   IsOptional = opt
                                       
                                   TypeName = pt
                                |})
                |> List.ofSeq                
        let decl =
            [
               yield sprintf "export interface %s {" (np typ)
               yield!
                    normalized
                    |> Seq.map (
                                   fun p ->
                                           sprintf "    %s%s : %s;" p.PropertyName p.IsOptional p.TypeName
                               )
               yield "};"
            ]
        let fieldMap =
            [
                yield "decodeMap("
                yield "    {"
                yield!
                    normalized
                    |> Seq.map (fun p -> sprintf "        %s:'%s'," p.PropertyName p.TypeKey)
                yield "    })"
            ]
        Some({
            Code = decl
            FromPlain = fieldMap
            ToPlain = fieldMap
        }, (fields |> List.ofArray |> List.map (fun p -> p.PropertyType)))
    | _ -> None
    
let nameArray (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsArrayLike t -> Some(sprintf "Array<%s>" (np t)) 
    | _ -> None
    
let genArray (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsArrayLike t ->
        let code = [sprintf "decodeArray('%s')" (np t)]
        Some({
            Code = []
            FromPlain = code
            ToPlain =  code    
        }, [t])
    | _ -> None    
    
let nameUnion (settings:GenerationSettings) (np: NameProvider) typ : string option =
    match typ with
    | IsUnion _ -> settings.TypeNameConverter typ |> Some
    | _ -> None
        
let genUnion (settings:GenerationSettings) (np: NameProvider) typ : CodeGeneratorResult<string> option =
    match typ with
    | IsUnion (Erased cases) ->
        let typeName = np typ
        let decl =
            cases
            |> Seq.map (settings.Serializer.CaseNameConverter >> sprintf "'%s'")
            |> String.concat " | "
        Some({
            Code = [sprintf "export type %s = %s;" typeName decl]
            FromPlain = []
            ToPlain = []
        }, [])
    | IsUnion (NotErased cases) ->
        let typeName = np typ
        let vpName = settings.Serializer.UnionValueFieldName typ
        let discriminator = settings.Serializer.UnionDiscriminatorProvider typ
        let caseGen (case: CaseKind) =
             
            match case with
            | NoValue ci ->
                let caseName = settings.Serializer.CaseNameConverter ci
                {|
                    CaseValue = caseName 
                    Code =
                        [
                          sprintf "interface %s%s {" typeName ci.Name
                          sprintf "    %s: '%s';" discriminator caseName
                          sprintf "}"
                        ] 
                    Func = ["id,"]
                    TypeName = sprintf "%s%s" typeName ci.Name
                    Types = []
                |}
            | SingleValue (ci, pi) ->
                let caseName = settings.Serializer.CaseNameConverter ci
                {|
                    CaseValue = caseName
                    Code =
                        [
                          sprintf "interface %s%s {" typeName ci.Name
                          sprintf "    %s: '%s';" discriminator (settings.Serializer.CaseNameConverter ci)
                          sprintf "    %s: %s;" vpName (np pi.PropertyType)
                          sprintf "}"
                        ] 
                    Func = [sprintf "decodeSingleUnion('%s', '%s')," vpName (np pi.PropertyType) ]
                    TypeName = sprintf "%s%s" typeName ci.Name
                    Types = [pi.PropertyType]
                |}
            | TupleValue (ci, pi) ->
                let caseName = settings.Serializer.CaseNameConverter ci
                let types = pi |> Array.map (fun p -> np p.PropertyType)
                let decl = types |> String.concat ", "
                let names = types |> Seq.map (fun s -> sprintf "'%s'" s) |> String.concat ", "
                {|
                    CaseValue = caseName
                    Code =
                        [
                          sprintf "interface %s%s {" typeName ci.Name
                          sprintf "    %s: '%s';" discriminator (settings.Serializer.CaseNameConverter ci)
                          sprintf "    %s: [%s];" vpName decl
                          sprintf "}"
                        ] 
                    Func = [sprintf "decodeTupleUnion('%s', [%s])," vpName names]
                    TypeName = sprintf "%s%s" typeName ci.Name
                    Types = pi |> Seq.map (fun p -> p.PropertyType) |> Seq.toList
                |}
            | NamedValues (ci, pi) ->
                let caseName = settings.Serializer.CaseNameConverter ci
                let normalized =
                        pi
                        |> Seq.map (fun p ->
                                        let opt, pt =
                                            match p.PropertyType with
                                               | IsOption o ->
                                                   match o with
                                                   | SkipInMaps s -> "?", np s 
                                                   | _ -> "", np p.PropertyType
                                               | _ -> "", np p.PropertyType
                                        {|
                                           PropertyName = settings.Serializer.MemberNameConverter (p :> MemberInfo)
                                           TypeKey  = np p.PropertyType
                                           IsOptional = opt
                                           TypeName = pt
                                        |})
                        |> List.ofSeq                
                let decl =
                    [
                       yield sprintf "interface %s%s {" typeName ci.Name
                       yield sprintf "    %s: '%s';" discriminator (settings.Serializer.CaseNameConverter ci)
                       yield!
                            normalized
                            |> Seq.map (
                                           fun p ->
                                                   sprintf "    %s%s : %s;" p.PropertyName p.IsOptional p.TypeName
                                       )
                       yield "};"
                    ]
                let fieldMap =
                    [
                        yield "decodeMap("
                        yield "    {"
                        yield!
                            normalized
                            |> Seq.map (fun p -> sprintf "        %s:'%s'," p.PropertyName p.TypeKey)
                        yield "    }),"
                    ]
                {|
                   CaseValue = caseName
                   Code = decl
                   Func = fieldMap
                   Types = pi |> Seq.map (fun p -> p.PropertyType) |> Seq.toList
                   TypeName = sprintf "%s%s" typeName ci.Name
                |}
        let cases1 = cases |> Array.map (fun p -> caseGen p)
        let typeNames =
            cases1
            |> Seq.map (fun p -> p.TypeName)
            |> String.concat " | "
        let code = [
            for i in cases1 do
                yield! i.Code
                yield ""
            yield ""
            yield sprintf "export type %s = %s;" typeName typeNames
        ]
        let tab s = "    " + s
        let mappers =
            cases1
            |> Seq.map (fun p -> p.CaseValue, p.Func |> List.map tab)
            |> Seq.collect (fun (k, f) -> [ yield sprintf "'%s':" k; yield! f ] )
            |> Seq.map tab
            |> (fun p -> [ yield "{"; yield! p; yield "}"  ])
            |> List.map tab
        let func = [
            yield sprintf "decodeUnion('%s'," discriminator
            yield! mappers
            yield ")"
        ]
        let types = cases1 |> Seq.collect (fun p -> p.Types) |> Seq.distinct |> Seq.toList
        Some({
            Code = code
            FromPlain = func
            ToPlain = func
        }, types)
    | _ -> None
        
let names settings np typ =
        [
            nameBool
            nameDate
            nameEnum
            nameNumber
            nameNumberOrString
            nameArray
            nameMap
            nameSet
            nameOption
            nameRecord
            nameString
            nameTuple
            nameUnion
        ] |> List.fold (fun st p -> st |> Option.orElseWith (fun () -> p settings np typ)) None
   
let gens settings np typ =
    [
        genBool
        genDate
        genEnum
        genNumber
        genNumberOrString
        genArray
        genMap
        genSet
        genOption
        genRecord
        genString
        genTuple
        genUnion
    ] |> List.fold (fun st p -> st |> Option.orElseWith (fun () -> p settings np typ)) None