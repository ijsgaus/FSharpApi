module FSharpApi.TypeScript.Sets

let generator = {
    Collect =
        fun typ ->
            match typ with
            | IsSetLike t ->
                Some [ t ]
            | _ -> None
    ProvideName =
        fun settings np typ ->
            let inner =
                (|IsSetLike|_|) typ
                |> Option.defaultWith (fun () -> invalidOp "Cannot be here")
                |> np
            { Name = { Namespace = ""; Name = sprintf "{ [key: string]: %s }" inner.Name.FullName }; NeedDecode = true }
    Generate =
        fun typ settings np cw ->
            let inner =
                (|IsSetLike|_|) typ
                |> Option.defaultWith (fun () -> invalidOp "Cannot be here")
                |> np
            let myName = np typ
            let vn = if inner.NeedDecode then sprintf "'%s'" inner.Name.FullName else "null"
            let encode = sprintf "$es6SetEncoder(%s)" vn
            let decode = sprintf "$es6SetDecoder(%s)" vn
            cw
            |> CodeWriter.addEncoder myName.Name.FullName [encode]
            |> CodeWriter.addDecoder myName.Name.FullName [decode]
}