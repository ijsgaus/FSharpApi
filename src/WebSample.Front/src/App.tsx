import React, {useCallback, useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';

const loadEffect = <T extends any>(loader: (setState: (value: T) => void) => Promise<any>, setState: (value: T) => void) => {
  return () => {
    // noinspection JSIgnoredPromiseFromCall
    loader(setState);
  }
}

const load = async (onLoad: (state: string[]) => void) => {
  const result = await (await fetch("/api/values")).json();
  onLoad(result);
};

export default () => {
  const [state, setState] = useState<string[]>([]);
  const effect = useCallback(loadEffect(load, setState), [setState])
  useEffect(effect,[])
  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <p>
            {state && JSON.stringify(state)}
          </p>
          <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
  )
}


//export default App;
