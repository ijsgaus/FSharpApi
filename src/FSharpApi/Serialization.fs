﻿namespace FSharpApi
open System
open System.Reflection
open FSharp.Reflection


type SerializerSettings =
    {
        MemberNameConverter : MemberInfo -> string
        CaseNameConverter: UnionCaseInfo -> string
        UnionDiscriminatorProvider: Type -> string
        EnumNameConverter: Type -> Option<FieldInfo -> string>
        UnionValueFieldName: Type -> string
    }

