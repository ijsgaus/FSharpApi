module FSharpApi.Json.FSharpTypes
open System
open System.Reflection
open System.Linq
open FSharp.Reflection
open System.Text.Json
open System.Text.Json
open TypeShape.Core
open FSharpApi

let generate = {
     new ISerializerGenerator with
         member __.Generate<'t>(settings, cache) =
            match shapeof<'t> with
            | Shape.FSharpRecord (:? ShapeFSharpRecord<'t> as s) ->
                let sers = s.Fields |> Array.map (makeFieldSerializer settings cache true)
                let ctor = FSharpValue.PreComputeRecordConstructor(typeof<'t>, true)
                let elementsMap = s.Fields |> Array.map (makeFieldReader settings cache) 
                Some (
                    wrapS<'t, _>(fun r prop writer ->
                            Utf8Writer.writeObject
                                (fun w -> sers |> Array.iter (fun p -> p r w))
                                prop
                                writer),
                    
                    wrapD(fun (el: JsonElement) ->
                            let map = el.EnumerateObject() |> Seq.map (fun p -> p.Name, p.Value) |> Map.ofSeq 
                            elementsMap |> Array.map (fun p -> p map) |> ctor |> unbox<'t>)
                )
                
            | Shape.FSharpUnion (:? ShapeFSharpUnion<'t> as shape) ->
                let discrim = settings.UnionDiscriminatorProvider typeof<'t>
                let discriminator = discrim |> JsonEncodedText.Encode
                let ctors = FSharpType.GetUnionCases(typeof<'t>)
                            |> Seq.map (fun p -> p.Tag, FSharpValue.PreComputeUnionConstructor(p, true))
                            |> Map.ofSeq
                match shape with
                | IsErasableUnion ->
                    let namesS = shape.UnionCases |>Array.map (fun p -> settings.CaseNameConverter p.CaseInfo |> JsonEncodedText.Encode)
                    let namesD = shape.UnionCases
                                 |> Seq.map (fun p -> settings.CaseNameConverter p.CaseInfo, p.CaseInfo.Tag)
                                 |> Map.ofSeq    
                    Some (
                        wrapS(
                            fun (p : 't) prop writer ->
                                let case = namesS.[shape.GetTag p]
                                Utf8Writer.writeJsonText case prop writer),
                        wrapD(
                            fun (el: JsonElement) ->
                                let str = el.GetString()
                                let tag = namesD.[str]
                                let ctor = ctors.[tag]
                                ctor([||]) |> unbox<'t>)
                    )
                | _ ->
                    let valueName = settings.UnionValueFieldName typeof<'t>
                    let mkUnionCaseSerializer (s : ShapeFSharpUnionCase<'t>) =
                        let fieldsPrinter =
                            match s with
                            | IsNoValueCase ->
                                fun r writer -> ()
                            | IsSingleValueCase single ->   
                                let s = makeNamedFieldSerializer cache valueName single
                                fun r writer ->
                                      s r writer
                            | IsTupleValueCase ->
                                let fieldPrinters = s.Fields |> Array.map (makeFieldSerializer settings cache false)
                                fun r writer ->
                                    writer.WriteStartArray(valueName)
                                    fieldPrinters |> Array.iter (fun p -> p r writer)
                                    writer.WriteEndArray()
                            | _ ->
                                let fieldPrinters = s.Fields |> Array.map (makeFieldSerializer settings cache true)
                                fun r writer ->
                                    fieldPrinters |> Array.iter (fun p -> p r writer)        
                        let caseName = settings.CaseNameConverter s.CaseInfo |> JsonEncodedText.Encode 
                        fun (u:'t) (writer: Utf8JsonWriter) ->
                            writer.WriteString(discriminator, caseName)
                            fieldsPrinter u writer
                    let mkUnionCaseDeserializer (s : ShapeFSharpUnionCase<'t>) =
                        let cctor = ctors.[s.CaseInfo.Tag];
                        match s with
                        | IsNoValueCase ->
                            fun map ->
                                cctor([||]) |> unbox<'t>
                        | IsSingleValueCase field ->
                            field.Accept {
                                new IReadOnlyMemberVisitor<'t, Map<string, JsonElement> -> 't> with
                                    member __.Visit<'f10>(shape: ReadOnlyMember<'t, 'f10>) =
                                        fun map ->
                                            let inner = cache.GetDeserializer<'f10>() (map.[valueName]) |> box
                                            cctor([| inner |]) |> unbox<'t>
                                        
                            }
                        | IsTupleValueCase ->
                            let fieldReaders = s.Fields |> Array.map (makeElementReader cache)
                            fun (map: Map<string, JsonElement>) ->
                                map.[valueName].EnumerateArray()
                                |> Seq.map2 (fun f v -> f v) fieldReaders
                                |> Seq.toArray
                                |> cctor 
                                |> unbox<'t>   
                        | _ ->
                            let fieldPrinters = s.Fields |> Array.map (makeFieldReader settings cache)
                            fun (map: Map<string, JsonElement>) ->
                                fieldPrinters
                                |> Array.map (fun p -> p map)
                                |> cctor
                                |> unbox<'t>
                    let caseWriters =shape.UnionCases |> Array.map mkUnionCaseSerializer
                    let caseReaders =
                        shape.UnionCases
                        |> Seq.map (fun p -> settings.CaseNameConverter p.CaseInfo, mkUnionCaseDeserializer p)
                        |> Map.ofSeq
                    Some (
                            fun (p: 't) (prop: JsonEncodedText voption) (writer: Utf8JsonWriter) ->
                                match prop with
                                | ValueSome name -> writer.WriteStartObject(name)
                                | ValueNone -> writer.WriteStartObject()
                                let wr = caseWriters.[shape.GetTag p]
                                wr p writer
                                writer.WriteEndObject()
                            ,
                        
                            fun (el: JsonElement) ->
                                let map = el.EnumerateObject() |> Seq.map (fun p -> p.Name, p.Value) |> Map.ofSeq
                                let d = map.[discrim]
                                let dv = d.GetString()
                                caseReaders.[dv] map
                    )
            | _ -> None
}            

