namespace FSharpApi.Json
open System
open System.Collections.Concurrent
open System.Text.Json
open System.Reflection
open FSharpApi

type Serializer<'t> = 't -> JsonEncodedText voption -> Utf8JsonWriter -> unit
type Deserializer<'t> = JsonElement -> 't 

type ISerializer =
    abstract GetSerializer<'t> : unit -> Serializer<'t>
    abstract GetDeserializer<'t> : unit -> Deserializer<'t>
    

type ISerializerGenerator =
    abstract Generate<'t> : settings: SerializerSettings * cache: ISerializer -> Option<Serializer<'t> * Deserializer<'t>>
