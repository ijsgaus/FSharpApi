module FSharpApi.Json.Nullables

open System
open System.Reflection
open System.Linq
open System.Text.Json
open TypeShape.Core
open FSharpApi

let generate = {
     new ISerializerGenerator with
         member __.Generate<'t>(_, cache) =
            match shapeof<'t> with
            | Shape.Nullable s ->
                Some (
                    s.Accept {
                        new INullableVisitor<Serializer<'t>> with
                            member __.Visit<'a when 'a : (new : unit -> 'a) and 'a :> ValueType and 'a : struct> () =
                                let exec (v : Nullable<'a>) (prop: JsonEncodedText voption) (writer: Utf8JsonWriter) =
                                    match prop, v |> Option.ofNullable with
                                    | ValueNone, Some v -> cache.GetSerializer<'a>() v ValueNone writer
                                    | ValueNone, None -> writer.WriteNullValue()
                                    | ValueSome _, Some v -> cache.GetSerializer<'a>() v prop writer
                                    | ValueSome name, None -> writer.WriteNull(name)
                                wrapS exec 
                    },
                    s.Accept {
                        new INullableVisitor<Deserializer<'t>> with
                            member __.Visit<'a when 'a : (new : unit -> 'a) and 'a :> ValueType and 'a : struct> () =
                                let exec (el: JsonElement) =
                                    if el.Type = JsonValueType.Null then
                                        Nullable()
                                    else
                                        Nullable(cache.GetDeserializer<'a>() el)
                                wrapD exec 
                    }
                )
            | _ -> None
}
