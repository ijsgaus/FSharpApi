﻿module FSharpApi.Json.PrimitiveTypes
open FSharpApi

open System
open System.Collections.Generic
open System.Reflection
open TypeShape.Core
open System.Text.Json


let generate = {
    new ISerializerGenerator with
        member __.Generate<'t>(_, _) =
            match shapeof<'t> with
            | Shape.Bool ->
                Some (
                    Utf8Writer.writeBool |> wrapS<'t, _>,
                    wrapD<'t, _>(fun (el: JsonElement) -> el.GetBoolean()) 
                ) 
            | Shape.Byte ->
                Some (
                    wrapS(fun (v:byte) -> Utf8Writer.writeInt32 (int v)),
                    wrapD(fun (el: JsonElement) -> el.GetInt32() |> byte)
                )
            | Shape.SByte ->
                Some (
                    wrapS(fun (v:sbyte) -> Utf8Writer.writeInt32 (int v)),
                    wrapD(fun (el: JsonElement) -> el.GetInt32() |> sbyte)
                )
            | Shape.Int16 ->
                Some (
                    wrapS(fun (v:int16) -> Utf8Writer.writeInt32 (int v)),
                    wrapD(fun (el: JsonElement) -> el.GetInt32() |> int16)
                )
            | Shape.UInt16 ->
                Some (
                    wrapS(fun (v:uint16) -> Utf8Writer.writeInt32 (int v)),
                    wrapD(fun (el: JsonElement) -> el.GetInt32() |> uint16)
                )
            | Shape.Int32 ->
                Some (
                    wrapS(Utf8Writer.writeInt32),
                    wrapD(fun (el: JsonElement) -> el.GetInt32())
                )
            | Shape.UInt32 ->
                Some (
                    wrapS(Utf8Writer.writeUInt32),
                    wrapD(fun (el: JsonElement) -> el.GetUInt32())
                )
            | Shape.Int64 ->
                Some (
                    wrapS(Utf8Writer.writeInt64),
                    wrapD(fun (el: JsonElement) ->
                            match el.Type with
                            | JsonValueType.String -> Int64.Parse(el.GetString())
                            | _ -> el.GetInt64())
                )
            | Shape.UInt64 ->
                Some (
                    wrapS(Utf8Writer.writeUInt64),
                    wrapD(fun (el: JsonElement) ->
                            match el.Type with
                                | JsonValueType.String -> UInt64.Parse(el.GetString())
                                | _ -> el.GetUInt64())
                )
            | Shape.Single ->
                Some (
                    wrapS(Utf8Writer.writeSingle),
                    wrapD(fun (el: JsonElement) -> el.GetSingle())
                )
            | Shape.Double ->
                Some (
                    wrapS(Utf8Writer.writeDouble),
                    wrapD(fun (el: JsonElement) -> el.GetDouble())
                )
            | Shape.Char ->
                Some (
                    wrapS(fun (v:char) -> Utf8Writer.writeString (v.ToString())),
                    wrapD(fun (el: JsonElement) ->
                            let str = el.GetString()
                            if str.Length <> 1 then
                                invalidOp "invalid char"
                             else str.[0])
                )
            | Shape.String ->
                Some (
                    wrapS(Utf8Writer.writeString),
                    wrapD(fun (el: JsonElement) -> el.GetString())
                )
            | Shape.Guid ->
                Some (
                    wrapS(Utf8Writer.writeGuid),
                    wrapD(fun (el: JsonElement) -> el.GetGuid())
                )
            | Shape.DateTime ->
                Some (
                    wrapS(Utf8Writer.writeDateTime),
                    wrapD(fun (el: JsonElement) -> el.GetDateTime())
                )
            | Shape.DateTimeOffset ->
                Some (
                    wrapS(Utf8Writer.writeDateTimeOffset),
                    wrapD(fun (el: JsonElement) -> el.GetDateTimeOffset())
                )
            | Shape.FSharpUnit ->
                Some (
                    wrapS(fun (_:unit) prop writer -> Utf8Writer.writeArray (fun _ -> ()) prop writer),
                    wrapD(fun (el: JsonElement) ->
                            match el.Type with
                            | JsonValueType.Array when el.GetArrayLength() = 0 -> ()
                            | _ -> invalidOp "no unit type")    
                                   
                )
            | Shape.TimeSpan ->
                Some (
                    wrapS(fun (b:TimeSpan) -> Utf8Writer.writeDouble b.TotalMilliseconds),
                    wrapD(fun (el: JsonElement) -> el.GetDouble() |> TimeSpan.FromMilliseconds)
                )
            | _ -> None
}    


        
