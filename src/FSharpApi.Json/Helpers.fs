[<AutoOpen>]
module FSharpApi.Json.Helpers
open System
open System.Collections.Generic
open System.Text.Json
open TypeShape.Core
open FSharpApi


let (|IsErasableUnion|_|) (shape:IShapeFSharpUnion) =
            if shape.UnionCases |> Array.forall (fun p -> p.Fields.Length = 0) then
                Some()
            else None
let (|IsNoValueCase|_|) (shape:ShapeFSharpUnionCase<'t>) =
    if shape.Fields.Length = 0 then Some() else None
let (|IsSingleValueCase|_|) (shape:ShapeFSharpUnionCase<'t>) =
    if shape.Fields.Length = 1 then Some(shape.Fields.[0]) else None
let (|IsTupleValueCase|_|) (shape:ShapeFSharpUnionCase<'t>) =
    if shape.Fields |> Seq.forall (fun p -> p.MemberInfo.Name.StartsWith("Item")) then Some() else None
    
let wrapS<'t, 'a> (a: Serializer<'a>) : Serializer<'t> =
    unbox<Serializer<'t>> a
    
let wrapD<'t, 'a> (a: Deserializer<'a>) : Deserializer<'t> =
    unbox<Deserializer<'t>> a

let makeFieldSerializer (settings: SerializerSettings) (cache: ISerializer) (writeProps: bool) (shape: IShapeReadOnlyMember<'dt>) : 'dt -> Utf8JsonWriter -> unit =
    shape.Accept {
        new IReadOnlyMemberVisitor<'dt, 'dt -> Utf8JsonWriter -> unit> with
            member __.Visit(shape: ReadOnlyMember<'dt, 'f>) =
                let propName =
                    if writeProps then
                        shape.MemberInfo
                        |> ValueOption.ofObj
                        |> ValueOption.map settings.MemberNameConverter
                        |> ValueOption.defaultValue "value"
                        |> JsonEncodedText.Encode
                        |> ValueSome
                    else ValueNone
                fun v writer ->
                    let value = shape.Get v
                    cache.GetSerializer<'f>() value propName writer
    }
    
let makeNamedFieldSerializer (cache: ISerializer) (propName: string) (shape: IShapeReadOnlyMember<'dt1>) (v: 'dt1) : Utf8JsonWriter -> unit =
    shape.Accept {
        new IReadOnlyMemberVisitor<'dt1, Utf8JsonWriter -> unit> with
            member __.Visit(shape: ReadOnlyMember<'dt1, 'f1>) =
                let propName =
                        propName
                        |> JsonEncodedText.Encode
                        |> ValueSome
                fun writer ->
                    let value = shape.Get v
                    cache.GetSerializer<'f1>() value propName writer
    }    
            
let makeElementReader (cache: ISerializer) (shape: IShapeReadOnlyMember<'dt>) =
    shape.Accept {
        new IReadOnlyMemberVisitor<'dt, JsonElement -> obj> with
             member __.Visit(shape: ReadOnlyMember<'dt, 'a>) =
                fun (el:  JsonElement) ->
                    cache.GetDeserializer<'a>() el |> box
    }
    
let makeFieldReader (settings: SerializerSettings) (cache: ISerializer) (shape: IShapeReadOnlyMember<'dt1>) =
    shape.Accept {
        new IReadOnlyMemberVisitor<'dt1, Map<string, JsonElement> -> obj> with
             member __.Visit(shape: ReadOnlyMember<'dt1, 'a1>) =
                let propName = settings.MemberNameConverter shape.MemberInfo
                match shapeof<'a1> with
                | Shape.FSharpOption s ->
                     s.Element.Accept {
                          new ITypeVisitor<Map<string, JsonElement> -> obj> with
                            member __.Visit<'aa> () =
                                fun map ->
                                    (match map |> Map.tryFind propName with
                                    | Some v -> cache.GetDeserializer<'aa>() v |> Some
                                    | None -> None) |> box 
                     }              
                | _ ->
                    fun map ->
                        cache.GetDeserializer<'a1>() map.[propName] |> box
    }    

[<Literal>]
let private MAX_SAFE_INTEGER = 9007199254740991L
[<Literal>]
let private MIN_SAFE_INTEGER = -9007199254740991L

let private MAX_SAFE_UINT = uint64 MAX_SAFE_INTEGER

module Utf8Writer =
    let writeBool v (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteBoolean(name, v)
        | ValueNone -> writer.WriteBooleanValue(v)
    let writeInt32 (v: int32) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteNumber(name, v)
        | ValueNone -> writer.WriteNumberValue(v)
    let writeUInt32 (v: uint32) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteNumber(name, v)
        | ValueNone -> writer.WriteNumberValue(v)
    let writeInt64 (v: int64) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name ->
            if v > MAX_SAFE_INTEGER || v < MIN_SAFE_INTEGER then
                writer.WriteString(name, string v)
            else
                writer.WriteNumber(name, v)
        | ValueNone ->
            if v > MAX_SAFE_INTEGER || v < MIN_SAFE_INTEGER then
                writer.WriteStringValue(string v)
            else
                writer.WriteNumberValue(v)
    let writeUInt64 (v: uint64) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name ->
            if v > MAX_SAFE_UINT then
                writer.WriteString(name, string v)
            else
                writer.WriteNumber(name, v)
        | ValueNone ->
            if v > MAX_SAFE_UINT then
                writer.WriteStringValue(string v)
            else
                writer.WriteNumberValue(v)
    let writeSingle (v: float32) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteNumber(name, v)
        | ValueNone -> writer.WriteNumberValue(v)
    let writeDouble (v: float) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteNumber(name, v)
        | ValueNone -> writer.WriteNumberValue(v)
    let writeString (v: string) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteString(name, v)
        | ValueNone -> writer.WriteStringValue(v)
    let writeJsonText (v: JsonEncodedText) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteString(name, v)
        | ValueNone -> writer.WriteStringValue(v)
    let writeGuid (v: Guid) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteString(name, v)
        | ValueNone -> writer.WriteStringValue(v)
    let writeDateTime (v: DateTime) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteString(name, v)
        | ValueNone -> writer.WriteStringValue(v)
    let writeDateTimeOffset (v: DateTimeOffset) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteString(name, v)
        | ValueNone -> writer.WriteStringValue(v)
    let writeBytes (v: byte[]) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteBase64String(name, ReadOnlySpan(v))
        | ValueNone -> writer.WriteBase64StringValue(ReadOnlySpan(v))
    
    let writeObject (f: Utf8JsonWriter -> unit) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteStartObject(name)
        | ValueNone -> writer.WriteStartObject()
        f writer
        writer.WriteEndObject()
        
    let writeArray (f: Utf8JsonWriter -> unit) (name: JsonEncodedText voption) (writer:Utf8JsonWriter) =
        match name with
        | ValueSome name -> writer.WriteStartArray(name)
        | ValueNone -> writer.WriteStartArray()
        f writer
        writer.WriteEndArray()
        
