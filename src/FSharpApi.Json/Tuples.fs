module FSharpApi.Json.Tuples
open System
open System.Reflection
open System.Linq
open FSharp.Reflection
open FSharpApi
open System.Text.Json
open TypeShape.Core


let generate = {
     new ISerializerGenerator with
         member __.Generate<'t>(settings, cache) =
            match shapeof<'t> with
            | Shape.Tuple (:? ShapeTuple<'t> as s) ->
                let fields = s.Elements |> Array.map (makeFieldSerializer settings cache false)
                let len = s.Elements.Length
                let ctor = FSharpValue.PreComputeTupleConstructor(typeof<'t>)
                let elements = s.Elements |> Array.map (makeElementReader cache)
                Some (
     
                    wrapS<'t, _>(fun r prop writer ->
                        Utf8Writer.writeArray (fun w -> fields |> Array.iter (fun p -> p r w)) prop writer),
                    wrapD(fun (el : JsonElement) ->
                            if el.GetArrayLength() <> len then
                                invalidOp "Array has other cardinality"
                            else
                                el.EnumerateArray()
                                |> Seq.map2 (fun f v -> f v) elements
                                |> Seq.toArray
                                |> ctor
                                |> unbox<'t>)
                )
            | _ -> None
}

