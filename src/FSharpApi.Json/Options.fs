module FSharpApi.Json.Options
open System
open System.Reflection
open System.Linq
open System.Text.Json
open TypeShape.Core
open FSharpApi

let generate = {
    new ISerializerGenerator with
        member __.Generate<'t>(_, cache) =
            match shapeof<'t> with
            | Shape.FSharpOption s ->
               Some (
                   s.Element.Accept {
                        new ITypeVisitor<Serializer<'t>> with
                            member __.Visit<'a> () =
                                let exec (v: 'a option) (prop: JsonEncodedText voption) (writer: Utf8JsonWriter) =
                                    match v with
                                    | Some v -> cache.GetSerializer<'a>() v prop writer
                                    | None ->
                                        match prop with
                                        | ValueSome _ -> ()
                                        | ValueNone -> writer.WriteNullValue()
                                wrapS exec
                
                   },
                   
                   s.Element.Accept {
                        new ITypeVisitor<Deserializer<'t>> with
                            member __.Visit<'a> () =
                                let exec (el: JsonElement) =
                                    if el.Type = JsonValueType.Null then
                                       None
                                    else
                                       Some (cache.GetDeserializer<'a>() el)
                                wrapD exec 
                    }
               )        
            | _ -> None
}
