module FSharpApi.Json.CommonCollections
open System
open System.Reflection
open System.Linq
open FSharp.Reflection
open System.Text.Json
open TypeShape.Core
open FSharpApi


let generate = {
     new ISerializerGenerator with
         member __.Generate<'t>(settings, cache) =
            match shapeof<'t> with
            | Shape.Dictionary s ->
                Some (
                        
                    s.Accept {
                        new IDictionaryVisitor<Serializer<'t>> with
                            member __.Visit<'K, 'V when 'K : equality>() =
                                match s.Key with
                                | Shape.String ->
                                    fun (m: System.Collections.Generic.Dictionary<'K, 'V>) (prop:JsonEncodedText voption)  (writer: Utf8JsonWriter) ->
                                        let el = cache.GetSerializer<'V>()
                                        match prop with
                                        | ValueSome name -> writer.WriteStartObject(name)
                                        | ValueNone -> writer.WriteStartObject()
                                        m
                                        |> Seq.iter (fun p -> el p.Value (p.Key.ToString() |> JsonEncodedText.Encode |> ValueSome) writer)
                                        writer.WriteEndObject()
                                    |> wrapS
                                | _ ->
                                    fun (m: System.Collections.Generic.Dictionary<'K, 'V>) (prop:JsonEncodedText voption)  (writer: Utf8JsonWriter) ->
                                        let key = cache.GetSerializer<'K>()
                                        let value = cache.GetSerializer<'V>()
                                        match prop with
                                        | ValueSome name -> writer.WriteStartArray(name)
                                        | ValueNone -> writer.WriteStartArray()
                                        m
                                        |> Seq.iter (fun p ->
                                                        writer.WriteStartArray();
                                                        key p.Key ValueNone writer
                                                        value p.Value ValueNone writer
                                                        writer.WriteEndArray();
                                                        )
                                        writer.WriteEndArray()
                                    |> wrapS
                    },
                    
                    s.Accept {
                        new IDictionaryVisitor<Deserializer<'t>> with
                            member __.Visit<'K, 'V when 'K : equality>() =
                                match s.Key with
                                | Shape.String ->
                                    fun (el : JsonElement) ->
                                        let sel = cache.GetDeserializer<'V>()
                                        el.EnumerateObject().ToDictionary( (fun p -> p.Name), (fun p -> sel p.Value) )
                                    |> wrapD    
                                    
                                | _ ->
                                    fun (el : JsonElement) ->
                                        let key = cache.GetDeserializer<'K>()
                                        let value = cache.GetDeserializer<'V>()
                                        (el.EnumerateArray()
                                        |> Seq.map (fun p -> p.EnumerateArray() |> Seq.toArray)
                                        |> Seq.map (fun p -> key p.[0], value p.[1])).ToDictionary(fst, snd) 
                                    |> wrapD
                    }
                )
            | Shape.Array s when s.Rank = 1 ->
                Some (
                    
                    s.Element.Accept {
                        new ITypeVisitor<Serializer<'t>> with
                            member __.Visit<'a>() =
                                fun (t: 'a[]) (prop:JsonEncodedText voption)  (writer: Utf8JsonWriter) ->
                                    let el = cache.GetSerializer<'a>()
                                    match prop with
                                    | ValueSome name -> writer.WriteStartArray(name)
                                    | ValueNone -> writer.WriteStartArray()
                                    t |> Array.iter (fun p -> el p ValueNone writer)
                                    writer.WriteEndArray()
                                |> wrapS
                    },
                    
                   s.Element.Accept {
                         new ITypeVisitor<Deserializer<'t>> with
                            member __.Visit<'a> () = // 'T = 'a []
                                fun (js: JsonElement) ->
                                    let el = cache.GetDeserializer<'a>()
                                    js.EnumerateArray() |> Seq.map (fun p -> el p) |> Array.ofSeq
                                    
                                |> wrapD
                    }
                )
            | _ -> None
}            
           