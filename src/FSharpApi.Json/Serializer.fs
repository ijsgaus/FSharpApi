module FSharpApi.Json.Serializer
open FSharpApi
open FSharpApi.Json
open System
open System.Reflection
open System.Collections.Concurrent

type private Serializer (settings: SerializerSettings, generator : ISerializerGenerator) =
    let cache = new ConcurrentDictionary<Type, obj * obj>()
    member private __.generate<'t> (_) =
                generator.Generate<'t>(settings, __)
                |> Option.map (fun (s, d) -> box s, box d)
                |> Option.defaultWith (fun () -> invalidOp <| sprintf "Unknown serialization type %s" typeof<'t>.FullName )
    interface ISerializer with
        member __.GetSerializer<'t>() =
            let (s, _) = cache.GetOrAdd(typeof<'t>, __.generate<'t>)
            unbox<Serializer<'t>> s
        member __.GetDeserializer<'t>() =
            let (_, d) = cache.GetOrAdd(typeof<'t>, __.generate<'t>)
            unbox<Deserializer<'t>> d
                
let serialize writer (value : 't) (serializer: ISerializer)  =
    serializer.GetSerializer<'t>() value ValueNone writer
let deserialize<'t> element  (serializer: ISerializer)  =
    serializer.GetDeserializer<'t>() element

let toCamelCase str = str |> String.mapi( fun i ch -> if i = 0 then Char.ToLower(ch) else ch)

let defaultSettings =
       {
            MemberNameConverter = fun p -> toCamelCase p.Name
            CaseNameConverter = fun p -> toCamelCase p.Name
            UnionDiscriminatorProvider = fun _ -> "$type"
            UnionValueFieldName = fun _ -> "payload"
            EnumNameConverter =
               fun t ->
                  if t.GetCustomAttribute<FlagsAttribute>() |> isNull then Some (fun f -> toCamelCase f.Name)
                  else None
        }

let create settings generators =
    let generator = {
        new ISerializerGenerator with
            member __.Generate<'t>(settings, cache) =
                let folder st (g : ISerializerGenerator) =
                    let orElse()  = g.Generate<'t>(settings, cache)
                    st |> Option.orElseWith orElse
                generators
                |> List.fold folder None
                
    }
    Serializer(settings, generator) :> ISerializer    
    

let Default =
    create defaultSettings [
        PrimitiveTypes.generate
        Enums.generate
        Options.generate
        Nullables.generate
        Tuples.generate
        FSharpCollections.generate
        CommonCollections.generate
        FSharpTypes.generate
    ] 

