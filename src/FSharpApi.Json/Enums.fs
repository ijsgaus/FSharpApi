module FSharpApi.Json.Enums
open FSharpApi
open System
open System.Reflection
open System.Linq
open System.Text.Json
open TypeShape.Core
 

let generate = {
    new ISerializerGenerator with
        member __.Generate<'t>(settings, _) =
            match shapeof<'t> with
            | Shape.Enum enum ->
                let fields = typeof<'t>.GetFields(BindingFlags.Static ||| BindingFlags.Public)
                match settings.EnumNameConverter typeof<'t> with
                | Some cvt ->
                    let fieldByNameS = fields.ToDictionary((fun p -> p.GetValue(null) :?> 't), fun p -> p)
                    let fieldByNameD = fields.ToDictionary((fun p -> p |> cvt),  (fun p -> p.GetValue(null) :?> 't))
                    Some (
                        wrapS<'t, _>(fun (v:'t) -> Utf8Writer.writeString (fieldByNameS.[v] |> cvt)),
                        wrapD<'t, _>(fun (el: JsonElement) -> fieldByNameD.[el.GetString()])
                    )
                | None ->
                    match enum.Underlying with
                    | Shape.UInt64 ->
                        Some (
                            wrapS(fun (v: 't) -> Utf8Writer.writeUInt64 (Convert.ToUInt64(v))),
                            wrapD(fun (el: JsonElement) ->
                                    let v =
                                        match el.Type with
                                        | JsonValueType.String -> UInt64.Parse(el.GetString())
                                        | _ -> el.GetUInt64()  
                                    unbox<'t> (Convert.ChangeType(v, enum.Underlying.Type)))  
                        )
                        
                    | _ ->
                        Some (
                            wrapS(fun (v: 't) -> Utf8Writer.writeInt64 (Convert.ToInt64(v))),
                            wrapD(fun (el: JsonElement) ->
                                    let v =
                                        match el.Type with
                                        | JsonValueType.String -> Int64.Parse(el.GetString())
                                        | _ -> el.GetInt64()
                                    unbox<'t> (Convert.ChangeType(v, enum.Underlying.Type))) 
                        )
            | _ -> None
}            
        
