module FSharpApi.Json.FSharpCollections
open System
open System.Reflection
open System.Linq
open FSharp.Reflection
open System.Text.Json
open TypeShape.Core
open FSharpApi


let generate = {
     new ISerializerGenerator with
         member __.Generate<'t>(settings, cache) =
            match shapeof<'t> with
            | Shape.FSharpList s ->
                Some (
                    s.Element.Accept {
                         new ITypeVisitor<Serializer<'t>> with
                            member __.Visit<'a> () = // 'T = 'a []
                                fun (l:'a list) (prop:JsonEncodedText voption)  (writer: Utf8JsonWriter) ->
                                    let el = cache.GetSerializer<'a>()
                                    let write w = l |> List.iter (fun p -> el p ValueNone w)
                                    Utf8Writer.writeArray write prop writer
                                |> wrapS
                    },
                    s.Element.Accept {
                         new ITypeVisitor<Deserializer<'t>> with
                            member __.Visit<'a> () = // 'T = 'a []
                                fun (js: JsonElement) ->
                                    js.EnumerateArray() |> Seq.map (fun p -> cache.GetDeserializer<'a>() p) |> List.ofSeq
                                |> wrapD
                    }
                )
            | Shape.FSharpMap s ->
                Some (
                    s.Accept {
                        new IFSharpMapVisitor<Serializer<'t>> with
                            member __.Visit<'K, 'V when 'K : comparison>() =
                                match s.Key with
                                | Shape.String ->
                                    fun (m: Map<'K, 'V>) (prop:JsonEncodedText voption)  (writer: Utf8JsonWriter) ->
                                        let el = cache.GetSerializer<'V>()
                                        match prop with
                                        | ValueSome name -> writer.WriteStartObject(name)
                                        | ValueNone -> writer.WriteStartObject()
                                        m
                                        |> Map.toSeq
                                        |> Seq.iter (fun (k, v) -> el v (k.ToString() |> JsonEncodedText.Encode |> ValueSome) writer)
                                        writer.WriteEndObject()
                                    |> wrapS
                                | _ ->
                                    fun (m: Map<'K, 'V>) (prop:JsonEncodedText voption)  (writer: Utf8JsonWriter) ->
                                        let key = cache.GetSerializer<'K>()
                                        let value = cache.GetSerializer<'V>()
                                        match prop with
                                        | ValueSome name -> writer.WriteStartArray(name)
                                        | ValueNone -> writer.WriteStartArray()
                                        m
                                        |> Map.toSeq
                                        |> Seq.iter (fun (k, v) ->
                                                        writer.WriteStartArray();
                                                        key k ValueNone writer
                                                        value v ValueNone writer
                                                        writer.WriteEndArray();
                                                        )
                                        writer.WriteEndArray()
                                    |> wrapS
                    },
                    s.Accept {
                        new IFSharpMapVisitor<Deserializer<'t>> with
                            member __.Visit<'K, 'V when 'K : comparison>() =
                                match s.Key with
                                | Shape.String ->
                                    fun (el : JsonElement) ->
                                        let sel = cache.GetDeserializer<'V>()
                                        el.EnumerateObject()
                                        |> Seq.map (fun p -> p.Name, sel p.Value)
                                        |> Map.ofSeq
                                    |> wrapD    
                                    
                                | _ ->
                                    fun (el : JsonElement) ->
                                        let key = cache.GetDeserializer<'K>()
                                        let value = cache.GetDeserializer<'V>()
                                        el.EnumerateArray()
                                        |> Seq.map (fun p -> p.EnumerateArray() |> Seq.toArray)
                                        |> Seq.map (fun p -> key p.[0], value p.[1])
                                        |> Map.ofSeq
                                    |> wrapD
                    }
                )
                        
            | Shape.FSharpSet s ->
                Some (
                    s.Accept {
                        new IFSharpSetVisitor<Serializer<'t>> with
                            member __.Visit<'a when 'a :comparison>() =
                                fun (l:Set<'a>) (prop:JsonEncodedText voption)  (writer: Utf8JsonWriter) ->
                                    let el = cache.GetSerializer<'a>()
                                    match prop with
                                    | ValueSome name -> writer.WriteStartArray(name)
                                    | ValueNone -> writer.WriteStartArray()
                                    l |> Seq.iter (fun p -> el p ValueNone writer)
                                    writer.WriteEndArray()
                                |> wrapS
                    },
                    s.Accept {
                        new IFSharpSetVisitor<Deserializer<'t>> with
                            member __.Visit<'a when 'a :comparison> () = // 'T = 'a []
                                fun (js: JsonElement) ->
                                    let el = cache.GetDeserializer<'a>()
                                    js.EnumerateArray()
                                    |> Seq.map (fun p -> el p)
                                    |> Set.ofSeq
                                |> wrapD
                    }
                )
            | _ -> None
}            
 
     