import { createScheduleApi, Schedules } from "./api";

const myRequest : Schedules.DailyRequest = {
    doctorId: 10,
    day: new Date(Date.now()),
}