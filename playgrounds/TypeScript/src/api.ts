export namespace Schedules {
    export interface DailyRequest {
        doctorId: number;
        day: Date;
    };

    export type DailyRequestError = "NoSuchDoctor" | "NotPlanned";

    export interface DailySchedule {

    }
}



export type Result<T, E> = { $type: "ok"; payload: T } | { $type: "error"; payload: E };

export interface IScheduleApi {
    getDaySchedule : ( request: Schedules.DailyRequest) => Promise<Result<Schedules.DailySchedule, Schedules.DailyRequestError>>;
}

export const createScheduleApi = () : IScheduleApi => {
    return {
        getDaySchedule: (request) => { throw Error(); }
    };
}
        
