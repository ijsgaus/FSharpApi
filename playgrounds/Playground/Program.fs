﻿// Learn more about F# at http://fsharp.org

open System
open System.IO
open System.Reflection
open System.Collections.Generic
open System.Text
open System.Text.Json
open FSharpApi.Json

let toJson<'t> config (v: 't) =
    let serialize1 (stream: MemoryStream) =
        use writer = new Utf8JsonWriter(stream, JsonWriterOptions())
        Serializer.serialize writer v config
        writer.Flush()
    use stream = new MemoryStream()  
    serialize1 stream
    stream.ToArray() |> Encoding.UTF8.GetString

type Enum1 =
    | One = 1
    | Two = 2

[<Flags>]
type Enum2 =
    | One = 1
    | Two = 2
    
type Record = {
    Field1: int
    Field2: string * Enum1
    Field3: int16 option
    Field4: Enum2 * Record option
}

type Union =
    | Case0
    | Case1 of int * bool
    | Case2 of me:int * other:string
    | Case3 of Record
    | Case4 of int list
    
type U1 =
    | A | B | C | Decimal

let check<'t> config (value: 't) =
    let s = toJson config value
    let doc = JsonDocument.Parse(s)
    let v = Serializer.deserialize<'t> doc.RootElement config 
    printfn "Type: %A\n  Value: %A\n  Serialized: %s\n  Deserialized: %A" typeof<'t> value s v

let serialization js =
    check js 32 
    check js 32L
    check js 9999999999999999999UL
    check js <| DateTime(1920, 01, 01)
    check js <| DateTimeOffset(1970, 01, 01,12, 05, 30, TimeSpan.FromHours(3.))
    check js <| Guid.NewGuid()
    check js <| TimeSpan.FromSeconds(1.)
    check js <| ()
    check js <| Enum1.Two
    check js <| Enum2.One
    check js <| Some 10
    check<int option> js <| None
    check js <| Nullable 10
    check js <| Nullable<int>()
    check js (55, "hellow")
    check js  struct (55, "hellow")
    check js {Field1 = 5; Field2 = "rty", Enum1.Two; Field3 = Some 567s; Field4 = Enum2.One, None}
    check js {Field1 = 5; Field2 = "rty", Enum1.Two; Field3 = None; Field4 = Enum2.One, None}
    let u1 = Case1(5, false)
    let u2 = Case2(5, "str")
    let u3 = Case3({Field1 = 5; Field2 = "rty", Enum1.Two; Field3 = None; Field4 = Enum2.One, Some {Field1 = 5; Field2 = "rty", Enum1.Two; Field3 = None; Field4 = Enum2.One, None}})
    let u4 = Case4 [ 1; 3 ]
    check js Case0
    check js u1
    check js u2
    check js u3
    check js u4
    
    check js [(1, "str")]
    check js [| 1; 2; 3; 5  |]
    
    check js A
    check js B
    check js C
    check js Decimal
    
    let map1 = [ "f1", 5; "f2", 6] |> Map.ofList
    let map2 = [ 10, 5; 12, 6] |> Map.ofList
    check js map1
    check js map2
    
//let printType (v : FSharperger.TypeScriptGenerator.TypeDef) =
//    match v.TypeCode with
//    | Some l -> printfn "%s" l.Value
//    | None -> ()
//    
//

open System.Collections.Generic

open FSharpApi.TypeScript
let tsGen js =
    generateTSTypes
        ({
            Serializer = js
            TypeNameConverter = fun t -> t.Name
        })
        Primitives.names
        Primitives.gens
    
    
type Type10 = {
    F1: Map<U1, string>
    F2: Dictionary<string, Record>
    F3: Set<Union>
}    
    
[<EntryPoint>]
let main argv =
    let js = Serializer.Default
    let str = tsGen Serializer.defaultSettings [typeof<Record>; typeof<U1>; typeof<Union>; typeof<Type10>]
    printfn "%s" str
    System.IO.File.WriteAllText("generated.ts", str)
    //serialization js
    0 // return an integer exit code
